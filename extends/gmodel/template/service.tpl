<?php
namespace {module}\service;

{common_name_space}

/**
 * {desc}
 * @author {author}
 * @email {email}
 * @date {date}
 */
class {className} {CommonService}{

    public function hello() {

        printf("Hello, World.");
    }

}