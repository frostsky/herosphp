<?php
namespace demo\service;

use common\service\CommonService;

/**
 * shop
 * @author yangjian
 * @email yangjian102621@gmail.com
 * @date 2017-03-20
 */
class ShopService extends CommonService {

    public function hello() {

        printf("Hello, World.");
    }

}